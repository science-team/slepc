<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML3.2 EN">
<HTML>
<HEAD> <link rel="canonical" href="https://slepc.upv.es/documentation/current/docs/manualpages/BV/BVDotQuadrature.html" />
<META NAME="GENERATOR" CONTENT="DOCTEXT">
<link rel="stylesheet" href="/slepc.css" type="text/css">
<TITLE>BVDotQuadrature</TITLE>
</HEAD>
<BODY BGCOLOR="FFFFFF">
   <div id="version" align=right><b>slepc-3.22.2 2024-12-02</b></div>
   <div id="bugreport" align=right><a href="mailto:slepc-maint@upv.es?subject=Typo or Error in Documentation &body=Please describe the typo or error in the documentation: slepc-3.22.2 v3.22.2 docs/manualpages/BV/BVDotQuadrature.html "><small>Report Typos and Errors</small></a></div>
<H1>BVDotQuadrature</H1>
Computes the projection terms required in the quadrature rule to approximate the contour integral. 
<H3><FONT COLOR="#883300">Synopsis</FONT></H3>
<PRE>
#include "slepcbv.h" 
<A HREF="https://petsc.org/release/manualpages/Sys/PetscErrorCode.html#PetscErrorCode">PetscErrorCode</A> <A HREF="../BV/BVDotQuadrature.html#BVDotQuadrature">BVDotQuadrature</A>(<A HREF="../BV/BV.html#BV">BV</A> Y,<A HREF="../BV/BV.html#BV">BV</A> V,<A HREF="https://petsc.org/release/manualpages/Sys/PetscScalar.html#PetscScalar">PetscScalar</A> *Mu,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> M,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> L,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> L_max,<A HREF="https://petsc.org/release/manualpages/Sys/PetscScalar.html#PetscScalar">PetscScalar</A> *w,<A HREF="https://petsc.org/release/manualpages/Sys/PetscScalar.html#PetscScalar">PetscScalar</A> *zn,<A HREF="https://petsc.org/release/manualpages/Sys/PetscSubcomm.html#PetscSubcomm">PetscSubcomm</A> subcomm,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> npoints,<A HREF="https://petsc.org/release/manualpages/Sys/PetscBool.html#PetscBool">PetscBool</A> useconj)
</PRE>
Collective
<P>
<H3><FONT COLOR="#883300">Input Parameters</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>Y       </B></TD><TD>&nbsp;- first basis vectors
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>V       </B></TD><TD>&nbsp;- second basis vectors
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>M       </B></TD><TD>&nbsp;- number of moments
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>L       </B></TD><TD>&nbsp;- block size
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>L_max   </B></TD><TD>&nbsp;- maximum block size
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>w       </B></TD><TD>&nbsp;- quadrature weights
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>zn      </B></TD><TD>&nbsp;- normalized quadrature points
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>subcomm </B></TD><TD>&nbsp;- subcommunicator layout
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>npoints </B></TD><TD>&nbsp;- number of points to process by the subcommunicator
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>useconj </B></TD><TD>&nbsp;- whether conjugate points can be used or not
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Output Parameter</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>Mu      </B></TD><TD>&nbsp;- computed result
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Notes</FONT></H3>
This is a generalization of <A HREF="../BV/BVDot.html#BVDot">BVDot</A>(). The resulting matrix Mu consists of M
blocks of size LxL (placed horizontally), each of them computed as
Mu_k = sum_j w_j*zn_j^k*V'*Y_j, where Y_j is the j-th panel of Y containing
the result of solving T(z_j)^{-1}*X for each integration point j. L_max is
the width of the panels in Y.
<P>
When using subcommunicators, Y is stored in the subcommunicators for a subset
of integration points. In that case, the computation is done in the subcomm
and then the final result is combined via reduction.
The value npoints is the number of points to be processed in this subcomm
and the flag useconj indicates whether symmetric points can be reused.
<P>

<P>
<H3><FONT COLOR="#883300">See Also</FONT></H3>
 <A HREF="../BV/BVDot.html#BVDot">BVDot</A>(), <A HREF="../BV/BVScatter.html#BVScatter">BVScatter</A>(), <A HREF="../BV/BVSumQuadrature.html#BVSumQuadrature">BVSumQuadrature</A>(), <A HREF="../RG/RGComputeQuadrature.html#RGComputeQuadrature">RGComputeQuadrature</A>(), <A HREF="../RG/RGCanUseConjugates.html#RGCanUseConjugates">RGCanUseConjugates</A>()
<BR><P><B></B><H3><FONT COLOR="#883300">Level</FONT></H3>developer<BR>
<H3><FONT COLOR="#883300">Location</FONT></H3>
</B><A HREF="../../../src/sys/classes/bv/interface/bvcontour.c.html#BVDotQuadrature">src/sys/classes/bv/interface/bvcontour.c</A>
<BR><BR><A HREF="./index.html">Index of all BV routines</A>
<BR><A HREF="../../../docs/manual.html">Table of Contents for all manual pages</A>
<BR><A HREF="../singleindex.html">Index of all manual pages</A>
</BODY></HTML>
