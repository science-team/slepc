<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML3.2 EN">
<HTML>
<HEAD> <link rel="canonical" href="https://slepc.upv.es/documentation/current/docs/manualpages/BV/BVMatLanczos.html" />
<META NAME="GENERATOR" CONTENT="DOCTEXT">
<link rel="stylesheet" href="/slepc.css" type="text/css">
<TITLE>BVMatLanczos</TITLE>
</HEAD>
<BODY BGCOLOR="FFFFFF">
   <div id="version" align=right><b>slepc-3.22.2 2024-12-02</b></div>
   <div id="bugreport" align=right><a href="mailto:slepc-maint@upv.es?subject=Typo or Error in Documentation &body=Please describe the typo or error in the documentation: slepc-3.22.2 v3.22.2 docs/manualpages/BV/BVMatLanczos.html "><small>Report Typos and Errors</small></a></div>
<H1>BVMatLanczos</H1>
Computes a Lanczos factorization associated with a matrix. 
<H3><FONT COLOR="#883300">Synopsis</FONT></H3>
<PRE>
#include "slepcbv.h"   
<A HREF="https://petsc.org/release/manualpages/Sys/PetscErrorCode.html#PetscErrorCode">PetscErrorCode</A> <A HREF="../BV/BVMatLanczos.html#BVMatLanczos">BVMatLanczos</A>(<A HREF="../BV/BV.html#BV">BV</A> V,<A HREF="https://petsc.org/release/manualpages/Mat/Mat.html#Mat">Mat</A> A,<A HREF="https://petsc.org/release/manualpages/Mat/Mat.html#Mat">Mat</A> T,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> k,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> *m,<A HREF="https://petsc.org/release/manualpages/Sys/PetscReal.html#PetscReal">PetscReal</A> *beta,<A HREF="https://petsc.org/release/manualpages/Sys/PetscBool.html#PetscBool">PetscBool</A> *breakdown)
</PRE>
Collective
<P>
<H3><FONT COLOR="#883300">Input Parameters</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>V </B></TD><TD>&nbsp;- basis vectors context
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>A </B></TD><TD>&nbsp;- the matrix
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>T </B></TD><TD>&nbsp;- (optional) the tridiagonal matrix
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>k </B></TD><TD>&nbsp;- number of locked columns
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>m </B></TD><TD>&nbsp;- dimension of the Lanczos basis, may be modified
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Output Parameters</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>beta </B></TD><TD>&nbsp;- (optional) norm of last vector before normalization
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>breakdown </B></TD><TD>&nbsp;- (optional) flag indicating that breakdown occurred
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Notes</FONT></H3>
Computes an m-step Lanczos factorization for matrix A, with full
reorthogonalization. At each Lanczos step, the corresponding Lanczos
vector is orthogonalized with respect to all previous Lanczos vectors.
This is equivalent to computing an m-step Arnoldi factorization and
exploting symmetry of the operator.
<P>
The first k columns are assumed to be locked and therefore they are
not modified. On exit, the following relation is satisfied
<P>
<pre>
                   A * V - V * T = beta*v_m * e_m^T
</pre>
<P>
where the columns of V are the Lanczos vectors (which are B-orthonormal),
T is a real symmetric tridiagonal matrix, and e_m is the m-th vector of
the canonical basis. On exit, beta contains the B-norm of V[m] before
normalization. The T matrix is stored in a special way, its first column
contains the diagonal elements, and its second column the off-diagonal
ones. In complex scalars, the elements are stored as <A HREF="https://petsc.org/release/manualpages/Sys/PetscReal.html#PetscReal">PetscReal</A> and thus
occupy only the first column of the <A HREF="https://petsc.org/release/manualpages/Mat/Mat.html#Mat">Mat</A> object. This is the same storage
scheme used in matrix <A HREF="../DS/DSMatType.html#DSMatType">DS_MAT_T</A> obtained with <A HREF="../DS/DSGetMat.html#DSGetMat">DSGetMat</A>().
<P>
The breakdown flag indicates that orthogonalization failed, see
<A HREF="../BV/BVOrthonormalizeColumn.html#BVOrthonormalizeColumn">BVOrthonormalizeColumn</A>(). In that case, on exit m contains the index of
the column that failed.
<P>
The values of k and m are not restricted to the active columns of V.
<P>
To create a Lanczos factorization from scratch, set k=0 and make sure the
first column contains the normalized initial vector.
<P>

<P>
<H3><FONT COLOR="#883300">See Also</FONT></H3>
 <A HREF="../BV/BVMatArnoldi.html#BVMatArnoldi">BVMatArnoldi</A>(), <A HREF="../BV/BVSetActiveColumns.html#BVSetActiveColumns">BVSetActiveColumns</A>(), <A HREF="../BV/BVOrthonormalizeColumn.html#BVOrthonormalizeColumn">BVOrthonormalizeColumn</A>(), <A HREF="../DS/DSGetMat.html#DSGetMat">DSGetMat</A>()
<BR><P><B></B><H3><FONT COLOR="#883300">Level</FONT></H3>advanced<BR>
<H3><FONT COLOR="#883300">Location</FONT></H3>
</B><A HREF="../../../src/sys/classes/bv/interface/bvkrylov.c.html#BVMatLanczos">src/sys/classes/bv/interface/bvkrylov.c</A>
<BR><BR><A HREF="./index.html">Index of all BV routines</A>
<BR><A HREF="../../../docs/manual.html">Table of Contents for all manual pages</A>
<BR><A HREF="../singleindex.html">Index of all manual pages</A>
</BODY></HTML>
