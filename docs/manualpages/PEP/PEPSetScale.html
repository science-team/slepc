<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML3.2 EN">
<HTML>
<HEAD> <link rel="canonical" href="https://slepc.upv.es/documentation/current/docs/manualpages/PEP/PEPSetScale.html" />
<META NAME="GENERATOR" CONTENT="DOCTEXT">
<link rel="stylesheet" href="/slepc.css" type="text/css">
<TITLE>PEPSetScale</TITLE>
</HEAD>
<BODY BGCOLOR="FFFFFF">
   <div id="version" align=right><b>slepc-3.22.2 2024-12-02</b></div>
   <div id="bugreport" align=right><a href="mailto:slepc-maint@upv.es?subject=Typo or Error in Documentation &body=Please describe the typo or error in the documentation: slepc-3.22.2 v3.22.2 docs/manualpages/PEP/PEPSetScale.html "><small>Report Typos and Errors</small></a></div>
<H1>PEPSetScale</H1>
Specifies the scaling strategy to be used. 
<H3><FONT COLOR="#883300">Synopsis</FONT></H3>
<PRE>
#include "slepcpep.h" 
<A HREF="https://petsc.org/release/manualpages/Sys/PetscErrorCode.html#PetscErrorCode">PetscErrorCode</A> <A HREF="../PEP/PEPSetScale.html#PEPSetScale">PEPSetScale</A>(<A HREF="../PEP/PEP.html#PEP">PEP</A> pep,<A HREF="../PEP/PEPScale.html#PEPScale">PEPScale</A> scale,<A HREF="https://petsc.org/release/manualpages/Sys/PetscReal.html#PetscReal">PetscReal</A> alpha,<A HREF="https://petsc.org/release/manualpages/Vec/Vec.html#Vec">Vec</A> Dl,<A HREF="https://petsc.org/release/manualpages/Vec/Vec.html#Vec">Vec</A> Dr,<A HREF="https://petsc.org/release/manualpages/Sys/PetscInt.html#PetscInt">PetscInt</A> its,<A HREF="https://petsc.org/release/manualpages/Sys/PetscReal.html#PetscReal">PetscReal</A> lambda)
</PRE>
Collective
<P>
<H3><FONT COLOR="#883300">Input Parameters</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>pep    </B></TD><TD>&nbsp;- the eigensolver context
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>scale  </B></TD><TD>&nbsp;- scaling strategy
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>alpha  </B></TD><TD>&nbsp;- the scaling factor used in the scalar strategy
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>Dl     </B></TD><TD>&nbsp;- the left diagonal matrix of the diagonal scaling algorithm
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>Dr     </B></TD><TD>&nbsp;- the right diagonal matrix of the diagonal scaling algorithm
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>its    </B></TD><TD>&nbsp;- number of iterations of the diagonal scaling algorithm
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>lambda </B></TD><TD>&nbsp;- approximation to wanted eigenvalues (modulus)
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Options Database Keys</FONT></H3>
<TABLE border="0" cellpadding="0" cellspacing="0">
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>-pep_scale &lt;type&gt; </B></TD><TD>&nbsp;- scaling type, one of &lt;none,scalar,diagonal,both&gt;
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>-pep_scale_factor &lt;alpha&gt; </B></TD><TD>&nbsp;- the scaling factor
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>-pep_scale_its &lt;its&gt; </B></TD><TD>&nbsp;- number of iterations
</TD></TR>
<TR><TD WIDTH=40></TD><TD ALIGN=LEFT VALIGN=TOP><B>-pep_scale_lambda &lt;lambda&gt; </B></TD><TD>&nbsp;- approximation to eigenvalues
</TD></TR></TABLE>
<P>
<H3><FONT COLOR="#883300">Notes</FONT></H3>
There are two non-exclusive scaling strategies, scalar and diagonal.
<P>
In the scalar strategy, scaling is applied to the eigenvalue, that is,
mu = lambda/alpha is the new eigenvalue and all matrices are scaled
accordingly. After solving the scaled problem, the original lambda is
recovered. Parameter 'alpha' must be positive. Use <A HREF="https://petsc.org/release/manualpages/Sys/PETSC_DETERMINE.html#PETSC_DETERMINE">PETSC_DETERMINE</A> to let
the solver compute a reasonable scaling factor, and <A HREF="https://petsc.org/release/manualpages/Sys/PETSC_CURRENT.html#PETSC_CURRENT">PETSC_CURRENT</A> to
retain a previously set value.
<P>
In the diagonal strategy, the solver works implicitly with matrix Dl*A*Dr,
where Dl and Dr are appropriate diagonal matrices. This improves the accuracy
of the computed results in some cases. The user may provide the Dr and Dl
matrices represented as <A HREF="https://petsc.org/release/manualpages/Vec/Vec.html#Vec">Vec</A> objects storing diagonal elements. If not
provided, these matrices are computed internally. This option requires
that the polynomial coefficient matrices are of <A HREF="https://petsc.org/release/manualpages/Mat/MATAIJ.html#MATAIJ">MATAIJ</A> type.
The parameter 'its' is the number of iterations performed by the method.
Parameter 'lambda' must be positive. Use <A HREF="https://petsc.org/release/manualpages/Sys/PETSC_DETERMINE.html#PETSC_DETERMINE">PETSC_DETERMINE</A> or set lambda = 1.0
if no information about eigenvalues is available. <A HREF="https://petsc.org/release/manualpages/Sys/PETSC_CURRENT.html#PETSC_CURRENT">PETSC_CURRENT</A> can also
be used to leave its and lambda unchanged.
<P>

<P>
<H3><FONT COLOR="#883300">See Also</FONT></H3>
 <A HREF="../PEP/PEPGetScale.html#PEPGetScale">PEPGetScale</A>()
<BR><P><B></B><H3><FONT COLOR="#883300">Level</FONT></H3>intermediate<BR>
<H3><FONT COLOR="#883300">Location</FONT></H3>
</B><A HREF="../../../src/pep/interface/pepopts.c.html#PEPSetScale">src/pep/interface/pepopts.c</A>
<BR><BR><A HREF="./index.html">Index of all PEP routines</A>
<BR><A HREF="../../../docs/manual.html">Table of Contents for all manual pages</A>
<BR><A HREF="../singleindex.html">Index of all manual pages</A>
</BODY></HTML>
