#include "petscsys.h"
#include "petscfix.h"
#include "petsc/private/fortranimpl.h"
/* slepcinit.c */
/* Fortran interface file */

/*
* This file was generated automatically by bfort from the C source
* file.  
 */

#ifdef PETSC_USE_POINTER_CONVERSION
#if defined(__cplusplus)
extern "C" { 
#endif 
extern void *PetscToPointer(void*);
extern int PetscFromPointer(void *);
extern void PetscRmPointer(void*);
#if defined(__cplusplus)
} 
#endif 

#else

#define PetscToPointer(a) (a ? *(PetscFortranAddr *)(a) : 0)
#define PetscFromPointer(a) (PetscFortranAddr)(a)
#define PetscRmPointer(a)
#endif

#include "slepcsys.h"
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define slepcgetversionnumber_ SLEPCGETVERSIONNUMBER
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define slepcgetversionnumber_ slepcgetversionnumber
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define slepcinitialized_ SLEPCINITIALIZED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define slepcinitialized_ slepcinitialized
#endif
#ifdef PETSC_HAVE_FORTRAN_CAPS
#define slepcfinalized_ SLEPCFINALIZED
#elif !defined(PETSC_HAVE_FORTRAN_UNDERSCORE) && !defined(FORTRANDOUBLEUNDERSCORE)
#define slepcfinalized_ slepcfinalized
#endif
/* Provide declarations for malloc/free if needed for strings */
#include <stdlib.h>


/* Definitions of Fortran Wrapper routines */
#if defined(__cplusplus)
extern "C" {
#endif
SLEPC_EXTERN void  slepcgetversionnumber_(PetscInt *major,PetscInt *minor,PetscInt *subminor,PetscInt *release, int *ierr)
{
CHKFORTRANNULLINTEGER(major);
CHKFORTRANNULLINTEGER(minor);
CHKFORTRANNULLINTEGER(subminor);
CHKFORTRANNULLINTEGER(release);
*ierr = SlepcGetVersionNumber(major,minor,subminor,release);
}
SLEPC_EXTERN void  slepcinitialized_(PetscBool *isInitialized, int *ierr)
{
*ierr = SlepcInitialized(isInitialized);
}
SLEPC_EXTERN void  slepcfinalized_(PetscBool *isFinalized, int *ierr)
{
*ierr = SlepcFinalized(isFinalized);
}
#if defined(__cplusplus)
}
#endif
