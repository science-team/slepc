#!/bin/sh -e

DEB_HOST_MULTIARCH=__DEB_HOST_MULTIARCH__

SONAME=__SLEPC_SONAME_VERSION__

SLEPC_VERSION=__SLEPC_VERSION__
SLEPC_ARCH=${DEB_HOST_MULTIARCH}
SLEPC_REAL_ARCH=${SLEPC_ARCH}-real
SLEPC_VERSION_NUMBER=$( echo ${SLEPC_VERSION} | sed "s/\.//g" )
SLEPC_SONAME_VERSION=__SLEPC_SONAME_VERSION__
SLEPC_NAME=slepc${SLEPC_SONAME_VERSION}

SLEPC_DIR_REAL=/usr/lib/slepcdir/${SLEPC_NAME}/${SLEPC_REAL_ARCH}

# Make alternatives links
if [ "$1" = "configure" ]; then

# alternative libslepc.so.multiarch is deprecated
# test if libslepc.so.multiarch is still in use, so remove old slepc alternatives
# This should be safe since Conflicts are set up with the dev package versions using deprecated libslepc.so.multiarch.
if update-alternatives --query slepc | grep libslepc.so.multiarch > /dev/null ; then
  echo Removing deprecated slepc alternatives using libslepc.so.multiarch.
  for alt in `update-alternatives --list slepc`; do
    update-alternatives --remove slepc ${alt}
  done
fi

# alternative default slepc (e.g. real or complex)
update-alternatives --install /usr/lib/slepc slepc ${SLEPC_DIR_REAL} ${SLEPC_VERSION_NUMBER}77 \
  --slave /usr/lib/${DEB_HOST_MULTIARCH}/libslepc.so libslepc.so /usr/lib/${DEB_HOST_MULTIARCH}/libslepc_real.so.${SLEPC_VERSION} \
  --slave /usr/include/slepc slepcinclude ${SLEPC_DIR_REAL}/include \
  --slave /usr/lib/${DEB_HOST_MULTIARCH}/pkgconfig/slepc.pc slepc.pc ${SLEPC_DIR_REAL}/lib/pkgconfig/slepc.pc \
  --slave /usr/lib/${DEB_HOST_MULTIARCH}/pkgconfig/SLEPc.pc SLEPc.pc ${SLEPC_DIR_REAL}/lib/pkgconfig/slepc.pc

# alternative base version of slepc real
update-alternatives --install /usr/lib/${DEB_HOST_MULTIARCH}/libslepc_real.so libslepc_real.so /usr/lib/${DEB_HOST_MULTIARCH}/libslepc_real.so.${SLEPC_VERSION} ${SLEPC_VERSION_NUMBER}77 \
  --slave /usr/lib/slepcdir/slepc-real slepc-real ${SLEPC_DIR_REAL}

# alternative slepc for this X.Y soname (e.g. real or complex)
update-alternatives --install /usr/lib/slepcdir/${SONAME} slepc${SONAME} ${SLEPC_DIR_REAL} ${SLEPC_VERSION_NUMBER}77

fi

#DEBHELPER#
